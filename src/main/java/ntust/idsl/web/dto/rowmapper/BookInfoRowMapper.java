package ntust.idsl.web.dto.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import ntust.idsl.web.dto.BookInfo;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author Carl Adler(C.A.)
 * */
public class BookInfoRowMapper implements RowMapper<BookInfo> {

	@Override
	public BookInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
		BookInfo bookInfo = new BookInfo();
		bookInfo.setBookId(rs.getInt("bookID"));
		bookInfo.setTitle(rs.getString("title"));
		bookInfo.setIsbn(rs.getString("isbn"));
		bookInfo.setAuthor(rs.getString("author"));
		bookInfo.setAbstraction(rs.getString("abstract"));
		bookInfo.setPrice(rs.getInt("price"));
		bookInfo.setPublisher(rs.getString("publisher"));
		return bookInfo;
	}
	
}
