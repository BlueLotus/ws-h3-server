package ntust.idsl.web.service;

import ntust.idsl.web.dto.BookInfo;

/**
 * @author Carl Adler(C.A.)
 * */
public interface BookService {
	public BookInfo[] listAll();
	public void test();
}
