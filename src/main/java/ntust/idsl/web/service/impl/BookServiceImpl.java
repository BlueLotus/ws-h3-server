package ntust.idsl.web.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ntust.idsl.web.dao.BookInfoDao;
import ntust.idsl.web.dto.BookInfo;
import ntust.idsl.web.service.BookService;

public class BookServiceImpl implements BookService{

	@Autowired
	BookInfoDao bookInfoDao;
	
	@Override
	public BookInfo[] listAll() {
		
		BookInfo[] bookList = new BookInfo[2];
		bookList[0] = setBookInfo(1, "Book1", "1259875632598", "Carl", "yooooo", 150, "NTUST");
		bookList[1] = setBookInfo(2, "sfsaf", "985748512365", "CCC", "yooo", 200, "sfasfsf");

		return bookList;
	}
	
	@Override
	public void test() {
		System.out.println("Start test method...");
		List<BookInfo> list = bookInfoDao.queryAllBookInfo();
		BookInfo[] array =  (BookInfo[]) list.toArray();
		for(int i = 0; i < array.length; i++) {
			printBookInfo(array[i]);
		}
	}
	
	private BookInfo setBookInfo(int bookId, String title, String isbn, String author, String abstraction, int price, String publisher){
		BookInfo bookInfo = new BookInfo();
		bookInfo.setBookId(bookId);
		bookInfo.setTitle(title);
		bookInfo.setIsbn(isbn);
		bookInfo.setAuthor(author);
		bookInfo.setAbstraction(abstraction);
		bookInfo.setPrice(price);
		bookInfo.setPublisher(publisher);
		
		return bookInfo;
	}
	
	private void printBookInfo(BookInfo bookInfo){
		System.out.println(bookInfo.getBookId());
		System.out.println(bookInfo.getTitle());
		System.out.println(bookInfo.getIsbn());
		System.out.println(bookInfo.getAuthor());
		System.out.println(bookInfo.getAbstraction());
		System.out.println(bookInfo.getPrice());
		System.out.println(bookInfo.getPublisher());
		System.out.println();
	}

}
