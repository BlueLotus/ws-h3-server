package ntust.idsl.web.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.support.JdbcDaoSupport;

import ntust.idsl.web.dao.BookInfoDao;
import ntust.idsl.web.dto.BookInfo;

/**
 * @author Carl Adler(C.A.)
 * */
public class BookInfoDaoImpl extends JdbcDaoSupport implements BookInfoDao{

	private final String queryForAllTheBookInfo = "select * from book_info";
	
	@Override
	public List<BookInfo> queryAllBookInfo() {
		List<BookInfo> bookInfoList = new ArrayList<BookInfo>();
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(queryForAllTheBookInfo);
		
		for(Map<String, Object> map : rows) {
			BookInfo bookInfo = new BookInfo();
			bookInfo.setBookId((Integer)map.get("bookID"));
			bookInfo.setTitle((String)map.get("title"));
			bookInfo.setIsbn((String)map.get("isbn"));
			bookInfo.setAuthor((String)map.get("author"));
			bookInfo.setAbstraction((String)map.get("abstract"));
			bookInfo.setPrice((Integer)map.get("price"));
			bookInfo.setPublisher((String)map.get("publisher"));
			bookInfoList.add(bookInfo);
		}
		
		return bookInfoList;
	}

}
