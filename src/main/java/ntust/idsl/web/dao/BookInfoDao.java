package ntust.idsl.web.dao;

import java.util.List;

import ntust.idsl.web.dto.BookInfo;

/**
 * @author Carl Adler(C.A.)
 * */
public interface BookInfoDao {
	public List<BookInfo> queryAllBookInfo();
}
